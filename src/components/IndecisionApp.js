import React from 'react';
import ParticlesBackground from './Particles';
import Header from './Header';
import Action from './Action';
import AddOption from './AddOption';
import Options from './Options';
import OptionModal from './OptionModal';

export default class IndecisionApp extends React.Component {
	state = {
		options: [],
		selectedOption: undefined,
		emptyErrors: [
			'Simply thinking of an option doesn\'t work...you actually have to type it.',
			'You add the options and I\'ll make the decision. Type something.'
		],
		repeatErrors: [
			'Don\'t play favorites...this option already exists.',
			'Entering this one again? Just choose this option yourself if you like it so much.'
		]
	}
	handleClearSelectedOption = () => {
		this.setState(() => ({ selectedOption: undefined }));		
	};
	handleDeleteOptions = () => {
		this.setState(() => ({ options: [] }));
	};
	handleDeleteOption = (optionToRemove) => {
		this.setState((prevState) => ({
			options: prevState.options.filter((option) => optionToRemove !== option )
		}));
	};
	handlePick = () => {
		const randomNum = Math.floor(Math.random() * this.state.options.length);
		const option = this.state.options[randomNum];
		this.setState(() => ({
			selectedOption: option	
		}));	
	};
	handleAddOption = (option) => {
		const randomEmpty = Math.floor(Math.random() * this.state.emptyErrors.length);
		const randomRepeat = Math.floor(Math.random() * this.state.repeatErrors.length);
		if (!option) {
			return this.state.emptyErrors[randomEmpty];
		} else if (this.state.options.indexOf(option) > -1) {
			return this.state.repeatErrors[randomRepeat];
		}
		this.setState((prevState) => ({ options: prevState.options.concat(option) }));
	};
	componentDidMount() {
		try {
			const json = localStorage.getItem('options');
			const options = JSON.parse(json);
	
			if (options) {
				this.setState(() => ({ options }));
			}	
		} catch (e) {
			//if invalid, fallback to default
		}
	}
	componentDidUpdate(prevProps, prevState) {
		if (prevState.options.length !== this.state.options.length) {
			const json = JSON.stringify(this.state.options);
			localStorage.setItem('options',json);
		}
	}
	render() {
		const subtitle = 'Put your life in the hands of a computer.';
		return (
			<div className="appWrapper">
				<div className="appBackground">
					<Header subtitle={subtitle} />
					<div className="appButtonWrap">
						<Action 
						hasOptions={this.state.options.length > 0}
						handlePick={this.handlePick}
						/>
					</div>
					<div className="appBodyWrap">
						<Options 
							options={this.state.options} 
							handleDeleteOptions={this.handleDeleteOptions}
							handleDeleteOption={this.handleDeleteOption}	
							hasOptions={this.state.options.length > 0}
						/>
						<AddOption 
						options={this.state.options} 
						handleAddOption={this.handleAddOption}
						/>
					</div>
				</div>					
				<OptionModal 
					selectedOption = {this.state.selectedOption}
					handleClearSelectedOption = {this.handleClearSelectedOption}
				/>
				<ParticlesBackground />
			</div>
		);
	}	
}

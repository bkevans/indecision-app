import React from 'react';
import Modal from 'react-modal';

const OptionModal = (props) => (
    <Modal
        isOpen={!!props.selectedOption}
        onRequestClose={props.handleClearSelectedOption}
        contentLabel="Selected Value"
        closeTimeoutMS={300}
        className="modal"
    >
        <h3 className="modal__title">Selected Option:</h3>
        {props.selectedOption && <p className="modal__body">{props.selectedOption}</p>}
        <button
            onClick={props.handleClearSelectedOption}
            className="button big-button modal__button"
        >
        Okie Dokie
        </button>
    </Modal>
);

Modal.setAppElement('body');

export default OptionModal;

![Indecision App Logo](public/images/indecision_logo.svg)

#### [Live Demo](http://indecision.bkevans.com)

A simple React application to help a user choose between various options randomly. Styling is written in Sass utilizing BEM methodology. Webpack handles compiling the code into a browser-friendly bundle with the help of Babel and node-sass, along with providing the development server with hot reloading.  

## Getting Started

### Prerequisites

Make sure that you have [Node.js](https://nodejs.org/en/) and [Yarn](https://yarnpkg.com/en/) installed.

To check the versions you have installed, run:  

```
node -v && yarn -v
```

### Installing

First you'll need to clone the source files onto your local machine. 

To clone you can run:

```
git clone https://bkevans@bitbucket.org/bkevans/indecision-app.git
```

Once cloned, move into the app directory:

```
cd indecision-app/
```
Now you'll need to install of the necessary dependencies to run the app locally using Yarn: 

```
yarn install
```

With all of the dependencies installed, you're ready to run a development build and spin up a development server for the app by running: 

```
yarn dev-server
```

This will build the app, and run it locally. The output from this command will provide a URL where the app can be accessed in your browser, e.g. *http://localhost:8080/*

Now that the app is up and running locally, you can crack open the /src directory and play around with the code to update components or styling. Changes will be rendered in the browser immediately as long as the development server is running. 

To shut down the dev server, simply press [cntrl + c] 

## Deployment

This project uses a Bitbucket deployment pipeline to deploy to an S3 bucket. 

In order to use this method, you'll need to set up an S3 bucket, generate a role with keys and write privelages for your S3 bucket, and set up the keys in your Bitbucket repo. A good guide for setting this up can be found [here](https://medium.com/@mabdullahabid/how-to-use-bitbucket-pipelines-for-continuous-deployment-to-amazon-s3-aeb4b3e1f282)

Once you have configured your S3 bucket and added your access keys to your bitbucket repo, simply edit the *bitbucket-pipelines.yml* file in this project with the region and endpoint for your S3 bucket.

The file is currently set up to deploy the /public directory whenever a git commit is made to the master branch and pushed to the repository.

To create a production build of this app, stop your dev server (if it is still running [cntrl + c], and run:  

```
 yarn build:prod
```

A production build will create a new *bundle.js* file in the public directory of the app. 

Simply commit your changes and push to your repo's master branch to initiate a deployment to your S3 bucket. Once complete you will have an updated app version that you can share with the world. 

## Built With

* [React](https://reactjs.org/) - The UI library used
* [Babel](https://babeljs.io/) - As a JavaScript compiler
* [Sass](https://sass-lang.com/) - To make things look interesting
* [Webpack](https://webpack.js.org/) - Used as a module bundler
* [react-particles-js](https://www.npmjs.com/package/react-particles-js) - A cool library which makes Particles JS accessible as a React Component

## License

This project is licensed under the MIT License

## Acknowledgments

This project was initially built while participating in an excellent [Udemy course](https://www.udemy.com/react-2nd-edition/learn/v4/overview) provided by Andrew Mead. A huge thank you to Andrew for sharing his knowledge and helping others grow their developement toolkit!

